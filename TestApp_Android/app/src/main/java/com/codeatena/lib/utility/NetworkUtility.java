package com.codeatena.lib.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.codeatena.app.testapp.App;

/**
 * Created by User on 7/2/2015.
 */
public class NetworkUtility {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public static NetworkUtility instance;

    public static NetworkUtility getInstance() {
        if (instance == null) {
            instance = new NetworkUtility();
        }
        return instance;
    }

    public int getConnectivityStatus() {
        ConnectivityManager cm = (ConnectivityManager) App.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }

        return TYPE_NOT_CONNECTED;
    }

    public String getConnectivityStatusString() {

        int conn = getConnectivityStatus();

        String status = null;

        if (conn == NetworkUtility.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetworkUtility.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == NetworkUtility.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }

        return status;
    }

    public boolean isNetworkAvailable() {
        if (getConnectivityStatus() != TYPE_NOT_CONNECTED) {
            return true;
        }
        return false;
    }
}
