package com.codeatena.lib.utility;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import com.codeatena.app.testapp.App;
import com.codeatena.app.testapp.R;

public class DialogUtility {

    public static DialogUtility instance;

    public static DialogUtility getInstance() {
        if (instance == null) {
            instance = new DialogUtility();
        }
        return instance;
    }

	public void showGeneralAlert(Context context, String title, String message) {
		
		if (context == null) return;
		
		new AlertDialog.Builder(context)
	      
		   .setTitle(title)
		   .setMessage(message)
		   .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) { 

		       }
		    })
		    .show();
	}
	
	public void show(String message) {

        Toast toast = null;

        if (toast == null && App.getInstance() != null) {
            toast = Toast.makeText(App.getInstance(), message, Toast.LENGTH_SHORT);
        }
        if (toast != null) {
            toast.setText(message);
            toast.show();
        }
    }

    public void show(int messageID) {

        Toast toast = null;

        if (toast == null && App.getInstance() != null) {
            toast = Toast.makeText(App.getInstance(), messageID, Toast.LENGTH_SHORT);
        }
        if (toast != null) {
            toast.setText(messageID);
            toast.show();
        }
    }

    public ProgressDialog getProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(R.string.please_wait));
        return progressDialog;
    }
    
    public ProgressDialog getProgressDialog(Context context, int messageID) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getResources().getString(messageID));
        return progressDialog;
    }

    public void showLong(String message) {

        Toast toast = null;

        if (message == null) {
            return;
        }
        if (toast == null && App.getInstance() != null) {
            toast = Toast.makeText(App.getInstance(), message, Toast.LENGTH_LONG);
        }
        if (toast != null) {
            toast.setText(message);
            toast.show();
        }
    }

    public Dialog createDialog(Context context, int titleID, int messageID,
            DialogInterface.OnClickListener positiveButtonListener,
            DialogInterface.OnClickListener negativeButtonListener) {
    	
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleID);
        builder.setMessage(messageID);
        builder.setPositiveButton(R.string.cancel, positiveButtonListener);
        builder.setNegativeButton(R.string.ok, negativeButtonListener);

        return builder.create();
    }

    public Dialog createDialog(Context context, int titleID, int messageID, View view,
            DialogInterface.OnClickListener positiveClickListener,
            DialogInterface.OnClickListener negativeClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleID);
        builder.setMessage(messageID);
        builder.setView(view);
        builder.setPositiveButton(R.string.ok, positiveClickListener);
        builder.setNegativeButton(R.string.cancel, negativeClickListener);

        return builder.create();
    }
}
