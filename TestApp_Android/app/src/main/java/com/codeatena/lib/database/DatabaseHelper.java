package com.codeatena.lib.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.codeatena.app.testapp.App;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "chatapp.db";

	public static DatabaseHelper instance;

	public static DatabaseHelper getInstance() {
		if (instance == null) {
			instance = new DatabaseHelper(App.getInstance());
		}
		return instance;
	}

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
//		Message.createTable(db);
//		User.createTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
//		Message.dropTable(db);
//		User.dropTable(db);
        onCreate(db);
	}
}