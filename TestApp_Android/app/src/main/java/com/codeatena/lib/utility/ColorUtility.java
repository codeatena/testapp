package com.codeatena.lib.utility;

import android.graphics.Color;

/**
 * Created by User on 7/10/2015.
 */
public class ColorUtility {

    public static ColorUtility instance = null;

    public static ColorUtility getInstance() {

        if (instance == null) {
            instance = new ColorUtility();
        }

        return instance;
    }

    public int convHexStringToInt(String hexString) {

        return Color.parseColor(hexString);
    }
}