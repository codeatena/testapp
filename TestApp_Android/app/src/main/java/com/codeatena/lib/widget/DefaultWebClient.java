package com.codeatena.lib.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.codeatena.lib.utility.DialogUtility;

public class DefaultWebClient extends WebViewClient {

    Context context;
    public ProgressDialog dlgLoading;

    public DefaultWebClient(Context _context) {
        context = _context;
    }

	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		// TODO Auto-generated method stub
        Log.e("webview url", url);

        super.onPageStarted(view, url, favicon);
        dlgLoading = DialogUtility.getInstance().getProgressDialog(context);
        dlgLoading.show();
	}

	public boolean shouldOverrideUrlLoading(WebView webView, String url) {
		// TODO Auto-generated method stub

        Log.e("webview url: ", url);
        webView.loadUrl(url);
		return true;
	}

    @Override
	public void onPageFinished(WebView view, String url) {
        if (dlgLoading.isShowing()) {
            dlgLoading.dismiss();
        }
        super.onPageFinished(view, url);
	}
	
	@Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

        Log.e("WEB_VIEW_TEST", "error code:" + errorCode);

        if (dlgLoading.isShowing()) {
            dlgLoading.dismiss();
        }

        super.onReceivedError(view, errorCode, description, failingUrl);
    }
}