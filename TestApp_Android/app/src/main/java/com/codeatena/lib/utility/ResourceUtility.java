package com.codeatena.lib.utility;

import com.codeatena.app.testapp.App;

/**
 * Created by User on 11/25/2015.
 */
public class ResourceUtility {

    public static ResourceUtility instance = null;

    public static ResourceUtility getInstance() {

        if (instance == null) {
            instance = new ResourceUtility();
        }

        return instance;
    }

    public int getIDFromResString(String res, String type){
        return App.getInstance().getResources().getIdentifier(res, type, App.getInstance().getPackageName());
    }
}