package com.codeatena.lib.utility;

import android.graphics.Point;
import android.graphics.Rect;

/**
 * Created by User on 7/17/2015.
 */
public class AreaUtility {

    public static AreaUtility instance;

    public static AreaUtility getInstance() {
        if (instance == null) {
            instance = new AreaUtility();
        }
        return instance;
    }

    public boolean rectHasPos(int letf, int top, int width, int height, float x, float y) {
        if (letf < x && x < letf + width) {
            if (top < y && y < top + height) {
                return true;
            }
        }
        return false;
    }

    public boolean circleHasPos(float cx, float cy, float r, float x, float y) {
        float d = ((float) x - cx) * ((float) x - cx) + ((float) y - cy) * ((float) y - cy);
        if (d > r * r) {
            return false;
        }
        return true;
    }
}