package com.codeatena.lib.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.codeatena.app.testapp.App;

public class PreferenceUtility {

	public static PreferenceUtility instance = null;

	public final String PREF_NAME = "chatapp_pref";
	public SharedPreferences sharedPreferences;
	public Editor prefEditor;

	public static PreferenceUtility getInstance() {

		if (instance == null) {
			instance = new PreferenceUtility();
		}

		return instance;
	}

	public PreferenceUtility() {
		sharedPreferences = App.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		prefEditor = sharedPreferences.edit();
	}
}