package com.codeatena.lib.utility;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.codeatena.app.testapp.App;

public class LocationUtility {

    public static LocationUtility instance;

    public static LocationUtility getInstance() {
        if (instance == null) {
            instance = new LocationUtility();
        }
        return instance;
    }

    public String getLatLng(Location currentLocation) {
        if (currentLocation != null) {
            return currentLocation.getLatitude() + ", " + currentLocation.getLongitude();
        } else {
            return "Null";
        }
    }
    
    public double getDistance(double latA, double lngA, double latB, double lngB) {
    	double distance;

    	Location locationA = createLocation(latA, lngA);
    	Location locationB = createLocation(latB, lngB);

    	distance = locationA.distanceTo(locationB);
    	
    	return distance;
    }

    public boolean isEnableGPS() {
        LocationManager service = (LocationManager) App.getInstance().getSystemService(Context.LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void setCurrentLocation(Location location) {
        PreferenceUtility.getInstance().prefEditor.putFloat("current_lat", (float) location.getLatitude());
        PreferenceUtility.getInstance().prefEditor.putFloat("current_lng", (float) location.getLongitude());
        PreferenceUtility.getInstance().prefEditor.commit();
    }

    public Location getCurrentLocation() {
        Location location = createLocation((double) PreferenceUtility.getInstance().sharedPreferences.getFloat("current_lat", 0.0f),
                (double) PreferenceUtility.getInstance().sharedPreferences.getFloat("current_lng", 0.0f));
        return location;
    }

    public Location createLocation(double lat, double lng) {
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }
}