package com.codeatena.lib.utility;

import android.content.Context;
import android.telephony.SmsManager;

public class SMSUtility {

	public static void sendSMS(Context context, String phone, String message) {
	    try {
	    	SmsManager smsManager = SmsManager.getDefault();
	    	smsManager.sendTextMessage(phone, null, message, null, null);
	    	DialogUtility.getInstance().show("SMS sent success");
	    } catch (Exception e) {
	    	DialogUtility.getInstance().show("SMS sent Failed");
	    	e.printStackTrace();
	    }
	}
}