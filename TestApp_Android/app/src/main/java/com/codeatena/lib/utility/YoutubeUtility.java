package com.codeatena.lib.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 6/14/2015.
 */
public class YoutubeUtility {

    public static YoutubeUtility instance;

    public static YoutubeUtility getInstance() {
        if (instance == null) {
            instance = new YoutubeUtility();
        }
        return instance;
    }

    public static String getYTIDFromYTUrl(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(".*(?:youtu.be\\/|v\\/|u\\/\\w\\/|embed\\/|watch\\?v=)([^#\\&\\?]*).*");
        Matcher matcher = pattern.matcher(ytUrl);

        if (matcher.matches()){
            vId = matcher.group(1);
        }

        return vId;
    }

    public static String getYTHTMLFromYTID(String ytID) {

        String html =
                "<iframe class=\"youtube-player\" "
                        + "style=\"border: 0; width: 100%; height: 95%;"
                        + "padding:0px; margin:0px\" "
                        + "id=\"ytplayer\" type=\"text/html\" "
                        + "src=\"http://www.youtube.com/embed/" + ytID + "\""
                        + "?fs=0\" frameborder=\"0\" " + "allowfullscreen autobuffer "
                        + "controls onclick=\"this.play()\">\n" + "</iframe>\n";

        return html;
    }
}
