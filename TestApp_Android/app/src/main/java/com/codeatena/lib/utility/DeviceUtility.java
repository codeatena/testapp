package com.codeatena.lib.utility;

import android.app.Activity;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by User on 7/8/2015.
 */
public class DeviceUtility {

    public static DeviceUtility instance = null;

    public static DeviceUtility getInstance() {

        if (instance == null) {
            instance = new DeviceUtility();
        }

        return instance;
    }

    public String getModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public String getDeviceName() {
        String deviceName = Build.SERIAL;
        return deviceName;
    }

    public String getOSVersion() {
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android " + sdkVersion;
    }

    public String getApiVersion() {
        String release = Build.VERSION.RELEASE;
        return release;
    }

    public String getYear() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        String year = Integer.toString(calendar.get(Calendar.YEAR));
        return year;
    }

    public Point getScreenSize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public String getScreenSizeString(Activity activity) {
        Point size = getScreenSize(activity);
        String str = Integer.toString(size.x) + "*" + Integer.toString(size.y);
        return str;
    }
}
