package com.codeatena.lib.utility;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.net.URISyntaxException;

import com.codeatena.app.testapp.App;

/**
 * Created by User on 7/10/2015.
 */
public class FileUtility {

    public static FileUtility instance = null;

    public static FileUtility getInstance() {

        if (instance == null) {
            instance = new FileUtility();
        }
        return instance;
    }

    public String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String downloadFile(String url, String path, FutureCallback<File> callback) {
        int k = url.lastIndexOf("/");
        String fileName = url.substring(k + 1);

        if (!path.substring(0, 1).equals("/")) {
            path = "/" + path;
        }
        if (!path.substring(path.length() - 1).equals("/")) {
            path += "/";
        }

        File dir = new File(Environment.getExternalStorageDirectory(), path);
        dir.mkdirs();
        String filePath = Environment.getExternalStorageDirectory().getPath() + path + fileName;
        Log.e("filepath", filePath);

        File file = new File(filePath);

        if (!file.exists()) {
            Ion.with(App.getInstance())
                    .load(url)
                    .write(file)
                    .setCallback(callback);
        }

        return filePath;
    }
}