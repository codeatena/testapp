package com.codeatena.app.testapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.koushikdutta.async.future.Future;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.codeatena.lib.utility.LocationUtility;
import com.codeatena.lib.utility.NetworkUtility;

public class TimerService extends Service {

    private static final String TAG = "TimerService";
    private boolean currentlyProcessingLocation = false;

    TimerTask timerTask = null;
    Timer timer = new Timer();

    final long PERIOD = 5 * 60 * 1000;

    public TimerService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e(TAG, "onStartCommand");

        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            startTimer();
        }

        return START_NOT_STICKY;
    }

    private void startTimer() {
        stopTimer();
        timerTask = new TimerTask() {
            @Override
            public void run() {

            }
        };
        timer = new Timer();
        timer.schedule(timerTask, PERIOD, PERIOD);
    }

    private void stopTimer() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        stopTimer();
        super.onDestroy();
    }
}