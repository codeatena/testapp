package com.codeatena.app.testapp.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import java.util.ArrayList;

import com.codeatena.lib.database.DatabaseHelper;
import com.codeatena.lib.utility.LocationUtility;
import com.codeatena.lib.utility.TimeUtility;

/**
 * Created by User on 7/29/2015.
 */
public class LocationModel {

    private static final String TAG = "LocationModel";

    public long id = -1;
    public Location location;
    public long datetime;

    final static String TABLE_NAME = "Location";

    public LocationModel(Location _location) {
        location = _location;
        datetime = TimeUtility.getInstance().getCurrentTimeStamp();
    }

    public LocationModel(long _id, Location _location, long _datetime) {
        id = _id;
        location = _location;
        datetime = _datetime;
    }

    public String getDateTimeString() {
        return TimeUtility.getInstance().getStringFromTimeStamp(datetime, "yyyy-MM-dd HH:mm:ss");
    }

    public String getInf() {
        String inf = id + ", " + LocationUtility.getInstance().getLatLng(location) + ", " + getDateTimeString();
        return inf;
    }

    public static void createTable(SQLiteDatabase db) {
        String sqlCreateTable = "CREATE TABLE " + TABLE_NAME + "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "lat REAL, " +
                "lng REAL, " +
                "datetime INTEGER" + ")";

        db.execSQL(sqlCreateTable);
    }

    public static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public static void addRecord(LocationModel locationModel) {
        SQLiteDatabase db = DatabaseHelper.getInstance().getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("lat", locationModel.location.getLatitude());
        values.put("lng", locationModel.location.getLongitude());
        values.put("datetime", locationModel.datetime);

        Log.e(TAG, "insert record: " + locationModel.getInf());
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public static ArrayList<LocationModel> getAllRecords() {
        ArrayList<LocationModel> arrRecords = new ArrayList<LocationModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = DatabaseHelper.getInstance().getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                long id = cursor.getLong(0);
                double lat = cursor.getDouble(1);
                double lng = cursor.getDouble(2);
                long datetime = cursor.getLong(3);
                Location location = LocationUtility.getInstance().createLocation(lat, lng);
                LocationModel record = new LocationModel(id, location, datetime);
                arrRecords.add(record);
            } while (cursor.moveToNext());
        }

        Log.e(TAG, "records get: " + arrRecords.size());
        cursor.close();
        db.close();
        return arrRecords;
    }

    public static void deleteRecord(LocationModel locationModel) {
        SQLiteDatabase db = DatabaseHelper.getInstance().getWritableDatabase();
        db.delete(TABLE_NAME, " id = '" + Long.toString(locationModel.id) + "'", null);
        Log.e(TAG, "delete record: " + locationModel.getInf());
        db.close();
    }
}