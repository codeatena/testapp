package com.codeatena.app.testapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

    LinearLayout linearLayout;
    GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

    }

    private void initValue() {

        Log.e("current system time", com.codeatena.lib.utility.TimeUtility.getInstance().getStringFromTimeStamp(System.currentTimeMillis() , "yyyy-MM-dd HH:mm:ss.SSS"));
        Log.e("utc time", com.codeatena.lib.utility.TimeUtility.getInstance().getCurrentUTCTimeAsFormat("yyyy-MM-dd HH:mm:ss"));
    }

    private void initEvent() {
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    Log.e("action", "down");
                }
                if (action == MotionEvent.ACTION_UP) {
                    Log.e("action", "up");
                }
                if (action == MotionEvent.ACTION_MOVE) {

                    final int historySize = event.getHistorySize();
                    final int pointerCount = event.getPointerCount();

                    for (int h = 0; h < historySize; h++) {
                        for (int p = 0; p < pointerCount; p++) {
                            Log.e("Pointer pos1", event.getPointerId(p) + ": " + event.getHistoricalX(p, h) + ", " + event.getHistoricalY(p, h) +
                                    ", " + event.getHistoricalPressure(p, h));
                        }
                    }
                    for (int p = 0; p < pointerCount; p++) {
                        Log.e("Pointer pos2", event.getPointerId(p) + ": " + event.getX(p) + ", " + event.getY(p) + ", " + event.getPressure(p));
                    }
                }

                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}